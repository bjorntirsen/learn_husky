const assert = require('assert');
const Calculator = require('../js/Calculator');

describe('Calculator', function () {
  beforeEach(function () {
    this.calc = new Calculator();
  });
  describe('#multiplicate()', function () {
    it('Should return 1 with factor 1 and current value 1', function () {
      this.calc.add(1);
      this.calc.multiply(1);
      assert.strictEqual(this.calc.getResult(), 1);
    });
    it('Should return 0 with factor 1 and current value 0', function () {
      this.calc.add(1);
      this.calc.multiply(0);
      assert.strictEqual(this.calc.getResult(), 0);
    });
    it('Should return 2 with factor -1 and current value 2', function () {
      this.calc.add(-2);
      this.calc.multiply(-1);
      assert.strictEqual(this.calc.getResult(), 2);
    });
  });
});
